/* 
ina219.c

*/

//#include "types.h"
#include "ina219.h"
#include "i2cmaster.h"

#define DEVICE_ADRESS_A 0x80 // 0x40 << 0 push
#define DEVICE_ADRESS_B 0x82
#define CONFIG_REG_VAL ( (uint16_t) 0x399F )
#define CAL_REG_VAL    ( (uint16_t) 4096 )
#define CAL_REG_ADDRES 0x5

/* Initialize ina219 IC */
void ina219_initA( void )
{
  /* Initialize I2C Controller */
  i2c_init(); //100k

  /* Set calibration word */
  i2c_start_wait( DEVICE_ADRESS_A + I2C_WRITE );
  i2c_write( CAL_REG_ADDRES ); // Calibration Register Address

  i2c_write( CAL_REG_VAL >> 8 ); // msb
  i2c_write( CAL_REG_VAL & 0xFF ); // lsb

  i2c_stop();

  /* Set config register */
  i2c_start_wait( DEVICE_ADRESS_A + I2C_WRITE );
  i2c_write( 0x0 ); // Calibration Register Address

  i2c_write( CONFIG_REG_VAL >> 8 ); // msb
  i2c_write( CONFIG_REG_VAL & 0xF ); // lsb

  i2c_stop();
}
void ina219_initB( void )
{
	/* Initialize I2C Controller */
	i2c_init(); //100k

	/* Set calibration word */
	i2c_start_wait( DEVICE_ADRESS_B + I2C_WRITE );
	i2c_write( CAL_REG_ADDRES ); // Calibration Register Address

	i2c_write( CAL_REG_VAL >> 8 ); // msb
	i2c_write( CAL_REG_VAL & 0xFF ); // lsb

	i2c_stop();

	/* Set config register */
	i2c_start_wait( DEVICE_ADRESS_B + I2C_WRITE );
	i2c_write( 0x0 ); // Calibration Register Address

	i2c_write( CONFIG_REG_VAL >> 8 ); // msb
	i2c_write( CONFIG_REG_VAL & 0xF ); // lsb

	i2c_stop();
}
/* Read a register from the ina219 */
static uint16_t ina219_read( uint8_t Reg,uint8_t ADD)
{
  uint16_t d_temp = 0;

  i2c_start_wait( ADD + I2C_WRITE );
  i2c_write( Reg ); 
  
  i2c_rep_start( ADD + I2C_READ ); // rep start to switch to reading
  // reading msb lsb frames
  d_temp = i2c_readAck() << 8;
  d_temp |= i2c_readAck();

  i2c_stop();

  return( d_temp );
}

/* Read load voltage in mV */
uint16_t ina219A_read_voltage( void )
{
  float temp_volt;

  /* Total Voltage = Shunt Voltage + BusVoltage */
  // shift to the right 3 go drop CNVR and OVF & multiply by LSB
  temp_volt =  ( ina219_read( 0x2 ,DEVICE_ADRESS_A) >> 3 ) * 4; // Bus
  temp_volt += ina219_read( 0x1,DEVICE_ADRESS_A ) * 0.01; // Shunt voltage mili
  temp_volt = temp_volt*0.01;
  return( temp_volt );
}
uint16_t ina219B_read_voltage( void )
{
	float temp_volt;

	/* Total Voltage = Shunt Voltage + BusVoltage */
	// shift to the right 3 go drop CNVR and OVF & multiply by LSB
	temp_volt =  ( ina219_read( 0x2 ,DEVICE_ADRESS_B ) >> 3 ) * 4; // Bus
	temp_volt += ina219_read( 0x1,DEVICE_ADRESS_B ) * 0.01; // Shunt voltage mili
	temp_volt = temp_volt*0.01;
	return( temp_volt );
}

/* Read current in mA */
uint16_t ina219A_read_current( void )
{	uint16_t temp_curr;
	i2c_start_wait( DEVICE_ADRESS_A + I2C_WRITE );
	i2c_write( CAL_REG_ADDRES ); // Calibration Register Address
	i2c_write( CAL_REG_VAL >> 8 ); // msb
	i2c_write( CAL_REG_VAL & 0xFF ); // lsb
	i2c_stop();
	temp_curr = ina219_read( 0x4,DEVICE_ADRESS_A );
	temp_curr = temp_curr*0.1;
	return( temp_curr );
}

uint16_t ina219B_read_current( void )
{	uint16_t temp_curr;
	i2c_start_wait( DEVICE_ADRESS_B + I2C_WRITE );
	i2c_write( CAL_REG_ADDRES ); // Calibration Register Address
	i2c_write( CAL_REG_VAL >> 8 ); // msb
	i2c_write( CAL_REG_VAL & 0xFF ); // lsb
	i2c_stop();
	temp_curr = ina219_read( 0x4,DEVICE_ADRESS_B );
	temp_curr = temp_curr*0.1;
	return( (temp_curr) );
}

/* Get Power */
uint16_t ina219A_read_power( void )
{	uint16_t temp_P;
	i2c_start_wait( DEVICE_ADRESS_A + I2C_WRITE );
	i2c_write( CAL_REG_ADDRES ); // Calibration Register Address
	i2c_write( CAL_REG_VAL >> 8 ); // msb
	i2c_write( CAL_REG_VAL & 0xFF ); // lsb
	i2c_stop();
	temp_P = ina219_read( 0x3,DEVICE_ADRESS_A);
	temp_P = temp_P*0.2;
	return(temp_P);
}
uint16_t ina219B_read_power( void )
{	uint16_t temp_P;
	i2c_start_wait( DEVICE_ADRESS_B + I2C_WRITE );
	i2c_write( CAL_REG_ADDRES ); // Calibration Register Address
	i2c_write( CAL_REG_VAL >> 8 ); // msb
	i2c_write( CAL_REG_VAL & 0xFF ); // lsb
	i2c_stop();
	temp_P = ina219_read( 0x3,DEVICE_ADRESS_B);
	temp_P = temp_P*0.2;
	return( temp_P);
}

