/* 
ina219.h

*/

#ifndef INA219_H
#define INA219_H

#include "types.h"
#include <avr/io.h>

/* Initialize ina219 IC */
void   ina219_initA();
void   ina219_initB();
// Read registers
//static uint16 ina219_read( uint8 u_Register ,uint8_t ADD);

/* Read a Volts from the ina219 */
uint16_t ina219A_read_voltage( void );
uint16_t ina219B_read_voltage( void );

/* Read current in mA */
uint16_t ina219A_read_current( void );
uint16_t ina219B_read_current( void );

// Get power
uint16_t ina219A_read_power( void );
uint16_t ina219B_read_power( void );

#endif

