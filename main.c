/*
	Project EEP61
	AC_DC converter Dynniq

*/

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#define F_CPU 16000000UL

#include <util/delay.h>


#include "ina219.h"
#include "lcdpcf8574.h"


//////////////////////////////////////////////////////////////////////////
void usart_init(unsigned int ubrr) {
	UBRR0H = (unsigned char)(ubrr >> 8);
	UBRR0L = (unsigned char)ubrr;
	UCSR0B = 1 << RXEN0 | 1 << TXEN0;
	UCSR0C = 0 << USBS0 | 3 << UCSZ00;
}
void uart_dec( unsigned char data ) {
	while(!(UCSR0A & (1 << UDRE0)));
	UDR0 = data;
}
void uart_ps( char *data) {
	unsigned char c;
	while(( c = *data++ )) {
		uart_dec(c);
		_delay_ms(1);
	}
}
void init_leds(void){
	// set pin as out mode
	DDRB = 1<<PORTB0 | 1<<PORTB1 | 1<<PORTB2 | 1<<PORTB3;
}
void check_sys(float volt , uint16_t amps){
	char buffer[30];
	if (volt >= 235){
		PORTB |= 1<<PORTB3;
		uart_ps("Relais Aan\n");
	}
	else{
		PORTB &= ~(1<<PORTB3);
	}
	if ((amps > 1300) && (amps < 6000)){
		// stroom beperkt op 3Amps i dont if we need to look at for In en OUT Amps
		PORTB |= 1<<PORTB1;
		sprintf(buffer,"%u amps ON\n",amps);
		uart_ps(buffer);
	}
	else{
		PORTB &= ~(1<<PORTB1);
	}
	memset(buffer,0,sizeof(buffer));
}
void show_volt_amp(float bus_voltA, float bus_voltB, uint16_t currentA, uint16_t currentB) {
	char buffer[40];
	//lcd_clrscr();
	
	// Row 1
	sprintf(buffer, "IN%0.1fV OUT%0.1fV\n", (bus_voltA*0.1), (bus_voltB*0.1));
	lcd_gotoxy(0,0);
	lcd_puts(buffer);
	
	//uart_ps(buffer);
	// trying to demp noise
	if (currentA > 2000)
	{
		currentA = 0;
	} 
	if (currentB > 2000)
	{
		currentB = 0;
	}
	// Row 2 currentB = 0.01 for 0.1ohm || 0.001 for 0.001 ohm
	sprintf(buffer, "IN%umA OUT%umA\n", (currentA),(currentB));
	lcd_gotoxy(0,1);
	lcd_puts(buffer);
	
	//uart_ps(buffer);
	memset(buffer,0,sizeof(buffer));
	//_delay_ms(1500);
}
void show_power(uint16_t p_in ,uint16_t p_out){
	char buffer[40];
	float n;
	if ((p_in > 0 && p_in <= 1000) && (p_out > 0 && p_out <=1000)){
		n     = (((float)p_out)/((float)p_in)) * 100;
	}
	else{
		n = 0;
	}
	//lcd_clrscr();
	float a = ((float)p_in)*0.01;
	// Row 1
	if (n >= 100){n = 0;}
	sprintf(buffer,"PIN%0.2fW  n%0.1f", (a),n);
	lcd_gotoxy(0,0);
	lcd_puts(buffer);
	lcd_gotoxy(15,0);
	lcd_puts("%");
	
	uart_ps(buffer);
	
	// Row 2
	float b = ((float)p_out)*0.01;
	sprintf(buffer,"POUT%0.2fW", (b));
	lcd_gotoxy(0,1);
	lcd_puts(buffer);
	
	uart_ps(buffer);
	memset(buffer,0,sizeof(buffer));
	//_delay_ms(2000);
	//lcd_clrscr();
}
//////////////////////////////////////////////////////////////////////////
int main(void)
{	
	init_leds();
	//init uart 115200
	usart_init(8);
    sei(); // global interrupts
	_delay_ms(1);
	ina219_initA();
	uart_ps("Check");
	ina219_initB();
	uart_ps("Check2");
    //init lcd
  
    lcd_init(LCD_DISP_ON); /* display on, cursor off */
    //lcd go home
    lcd_home();
	lcd_led(0); //set on with 0
	
    while(1) 
	{	
		// Get the voltage and current
		float bus_voltA = ina219A_read_voltage();
		float bus_voltB = ina219B_read_voltage();
		uint16_t current_A = ina219A_read_current();
		uint16_t current_B = ina219B_read_current();
		//check_sys(bus_voltA,current_B);
		for (uint8_t i = 0; i < 10; i++) {
			show_volt_amp(bus_voltA, bus_voltB, current_A, current_B);
			_delay_ms(100);
			bus_voltA = ina219A_read_voltage();
			current_A = ina219A_read_current();
			current_B = ina219B_read_current();
			check_sys(bus_voltA,current_B);
		}
		//show_volt_amp(bus_voltA, bus_voltB, current_A, current_B);
		
		// Get power from measurement device
		uint16_t p_in  = ina219A_read_power();
		uint16_t p_out = ina219B_read_power();
		lcd_clrscr();
		for (uint8_t i = 0; i<10;i++)
		{	 show_power(p_in, p_out);
			 _delay_ms(100);
			 bus_voltA = ina219A_read_voltage();
			 current_B = ina219B_read_current();
			 check_sys(bus_voltA,current_B);
		}
		lcd_clrscr();
		//show_power(p_in, p_out);
    }
}



//////////////////////////////////////////////////////////////////////////
//JUNK
//led = !led; //invert led for next loop
//#define LCD_DATA0_PIN    0            /**< pin for 4bit data bit 0  */
//#define LCD_DATA1_PIN    1            /**< pin for 4bit data bit 1  */
//#define LCD_DATA2_PIN    2            /**< pin for 4bit data bit 2  */
//#define LCD_DATA3_PIN    3            /**< pin for 4bit data bit 3  */
//#define LCD_RS_PIN       6            /**< pin  for RS line         */
//#define LCD_RW_PIN       5            /**< pin  for RW line         */
//#define LCD_E_PIN        4            /**< pin  for Enable line     */
//#define LCD_LED_PIN      7            /**< pin  for Led             */
